package tests;

import com.codeborne.selenide.Configuration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;

import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.Selenide.open;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BaseTest {

    @BeforeAll
    void setup() {
        Configuration.browser = "chrome";
    }

    @BeforeEach
    void openApp() {
        open("https://sinoptik.ua");
    }

    @AfterEach
    void closeDriver() {
        close();
    }


}
