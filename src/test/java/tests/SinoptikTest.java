package tests;

import com.codeborne.selenide.SelenideElement;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.SinoptikPage;
import utils.Helpers;

import static org.assertj.core.api.Assertions.assertThat;
import static pages.SinoptikPage.Day;

public class SinoptikTest extends BaseTest {

    /*
    open sinoptik.ua website
    search 'Драгобрат' in search field
    open 'Воскресенье' tab
    check that 'Воскресенье' tab is opened
    check that 'Давление, мм' row contains values in range [600,700]
     */

    private SinoptikPage sinoptikPage = new SinoptikPage();

    @Test
    @DisplayName("Check if pressure is in range [600, 700] on Sunday in Drahobrat")
    void checkIfPressureIsBetween600And700OnSunday() {
        String city = "Драгобрат";
        sinoptikPage.search(Helpers.cyrillic(city));
        sinoptikPage.selectDay(Day.Sunday);
        for (SelenideElement pressureElement : sinoptikPage.getPressures()) {
            int pressure = Integer.parseInt(pressureElement.getText());
            assertThat(pressure).as("Check if pressure is in range [600, 700]").isBetween(600, 700);
        }
    }

}
