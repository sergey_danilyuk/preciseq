package pages;

import com.codeborne.selenide.SelenideElement;
import utils.Helpers;

import java.util.List;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.openqa.selenium.By.className;
import static org.openqa.selenium.By.xpath;

public class SinoptikPage extends BasePage {

    public enum Day {
        Monday("Понедельник"),
        Tuesday("Вторник"),
        Wednesday("Среда"),
        Thursday("Четверг"),
        Friday("Пятница"),
        Saturday("Суббота"),
        Sunday("Воскресенье");

        Day(String xpath) {
            this.xpath = xpath;
        }

        private String xpath;
    }

    private SelenideElement searchField = $("#search_city");
    private SelenideElement searchButton = $(className("search_city-submit"));
    private SelenideElement daysTabs = $(xpath("//div[@class='tabs']"));
    private SelenideElement weatherInCity = $(xpath("//div[contains(@class, 'cityName')]/h1"));
    private SelenideElement dayInCalendar = $(".infoDayweek");
    private List<SelenideElement> pressures = $$(xpath("(//div[@id='bd4c']//tr[@class='gray'])[1]/td"));

    public void search(String city) {
        log.debug(String.format("Searching: %s and check if page is loaded", city));
        searchField.sendKeys(city);
        searchButton.click();
        weatherInCity.shouldHave(text(city));
    }

    public void selectDay(Day day) {
        String cyrillicDay = Helpers.cyrillic(day.xpath);
        log.debug(String.format("Click on: %s and check if page is loaded", cyrillicDay));
        daysTabs.$(xpath(String.format("//a[text()='%s']", cyrillicDay))).click();
        dayInCalendar.shouldHave(text(cyrillicDay));
    }

    public List<SelenideElement> getPressures() {
        return pressures;
    }
}
