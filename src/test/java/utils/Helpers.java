package utils;

import java.io.UnsupportedEncodingException;

public class Helpers {

    public static String cyrillic(String text) {
        try {
            byte bytes[] = text.getBytes();
            return new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
