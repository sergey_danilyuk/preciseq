Make sure you have Maven 3 and Java 8 installed.
Check out this repository and choose any way to launch it:
- In a terminal run **mvn clean test** (in the project folder)
- Add this project as a Maven project and run the class **SinoptikTest** (/src/test/tests) in Intellij Idea

